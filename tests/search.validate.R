library(RSUS)

check_search <- function(str, pattern) {
	cat(sprintf("%s %s \n", str, pattern))
	stree <- new(suffix_tree, str)
	result <- stree$search(pattern)
	print(result)
	for(i in 1:(nchar(str) - nchar(pattern) + 1)) {
		if (i %in% result) {
			stopifnot(substr(str, i, i + nchar(pattern) - 1) == pattern)
		} else {
			stopifnot(substr(str, i, i + nchar(pattern) - 1) != pattern)
		}
	}
	invisible(NULL)
}


check_search("banana", "a")
check_search("banana", "b")
check_search("banana", "n")
check_search("banana", "e")
check_search("b", "e")
check_search("b", "b")
check_search("banana", "na")
check_search("banana", "an")
check_search("banana", "ban")
check_search("banana", "bna")

check_search("aaaaabbaabbbbbaaaabbbabbaabbbabbabbaaabababaaabaaabbbbbabaabaaaababbbbbbbababbabbabaaaabbabaaaabaabbbbabbbabbaaaaaaabaaaabbaaabbabbbaaabbaaaaababbabaaabaaabbbbbbaabababbbaabbabbbaaaaaaaabbbbbbababaabbababbbaaababbbbabaabaabaabbbabbabaababbabaaabbbbbbaaaaababababbaaabbbaabaabbbabbaabbbbbbbbababbabbbbabbbbaaabababaaaaabaababaaabbabbaabbbabbabbbabbbbabaaaabaababaaabbabbbaabbaaabbbbbababbbbaaaaaaaaabbaaaabaaaaaaabbbbbaababaabbbbabaabaabaabaabbaabaaaaabababababbbabaabbbbbabbbbaaabbaaababaabaaabbbbbbbaaaabbabbbaababaaaaababbaaabbbbbabbbbabbaabaaabbaabbbaababaaababbabbbbabbaaaabbbbaabbabbaaabbabaabbababbbababbbbabbbaaaabbbababbabbbabbabbaaabbbbbabbaababaaabbaabbbaaaaabbbababaabbbbaabaaababababaaaaaaabbabbbbaaabbbbbabbbbabaabbaabaabbbbaabbbabbbbbaaabaaaaabaaabbbababbbaababbaabbbbbabbbaaaaabaabbabbabbaaabaabaabbabaaaabbbabbbbbbabaabbabbbbabbbaabbbaaabbbababbabbbabaaaaababaabbababaabbaabaabbbbaaababaaaaaabaaababbbabbbabaabbaaababbaabaababbababbbbbbabbabbbbbabaaaabbabbbabaaaaaababbabbbabaabbbbbba", "baabbabaaa")
# stop("stop")
source("gen_random_string.R")
for(i in 1:100) {
	check_search(gen_random_string(100), paste(sample(c("b", "a"), 4, TRUE), collapse=""))
}

for(i in 1:100) {
	check_search(gen_random_string(1000), gen_random_string(10))
}
