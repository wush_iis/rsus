check_lsus <- function(s) {
	stree <- new(suffix_tree, s)
	result <- stree$lsus( 1:nchar(s) )
	for(i in seq_along(result)) {
		if (is.na(result[i])) {
			pattern <- substr(s, i, nchar(s))
			stopifnot(length(stree$search(pattern)) > 1)
			next
		}
		pattern <- substr(s, i, i + result[i] - 1)
		stopifnot(length(stree$search(pattern)) == 1)
		if (nchar(pattern) > 1) {
			stopifnot(length(stree$search(substr(pattern, 1, nchar(pattern) - 1))) > 1)
		}
	}
	invisible(NULL)
}

library(RSUS)
stree <- new(suffix_tree, "banana")
result <- stree$lsus( 1:6 )
stopifnot(result[1:3] == c(1, 4, 3))
stopifnot(sum(is.na(result)) == 3)


source("gen_all_string.R")
for(i in 1:8) {
	src <- gen_all_string(n=i, src=src)
	pb <- txtProgressBar(max = length(src))
	for(i in seq_along(src)) {
		check_lsus(src[i])
		setTxtProgressBar(pb, i)
	}
	close(pb)
}

source("gen_random_string.R")
for(i in 1:100) {
	src <- gen_random_string(10000)
	check_lsus(src)
}
