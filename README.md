RSUS
========================================================

RSUS is a R package which implements our paper in ICDE 2013: On Shortest Unique Substring Queries.

# Enviornment

OS:

- Ubuntu 12.10

Dependencies:

- R 3.0.1
- Rcpp 0.10.3

# Installation

## Under R: devtools

Please make sure that R package `Rcpp` is installed.


```r
library(devtools)
install_bitbucket(repo = "rsus", username = "wush_iis")
```

## Under command line

```sh
git clone https://bitbucket.org/wush_iis/rsus.git
R CMD INSTALL rsus
```

# Usage

## Construct the suffix tree


```r
library(RSUS)
```

```
## Loading required package: Rcpp
## Loading required package: methods
```



```r
stree <- new(suffix_tree, "banana")
```


## Query LSUS


```r
# compute the length of LSUS at position 1 to 6
lsus.length <- stree$lsus(1:6)
lsus.length  # If the LSUS does not exist, the length will be `NA`
```

```
## [1]  1  4  3 NA NA NA
```



```r
substring("banana", 1:6, lsus.length)  # show the LSUSs
```

```
## [1] "b"   "ana" "n"   NA    NA    NA
```


## Query SUS


```r
sus <- stree$sus_baseline(1:6)  # query the left-most SUS at position 1 to 6
substring("banana", sus$head, sus$tail)  # show the SUSs
```

```
## [1] "b"    "ba"   "ban"  "nan"  "nan"  "nana"
```


## CTOQA


```r
sus.ctoqa <- stree$sus_ctoqa()  # query SUSs at all position
substring("banana", sus.ctoqa$head, sus.ctoqa$tail)
```

```
## [1] "b"    "ba"   "ban"  "nan"  "nan"  "nana"
```


## Example based on txt file

Suppose there is a text file named *DESCRIPTION*, the following script will read it into memory and measure the computation time of `sus_baseline` and `ctoqa`. 


```r
src <- paste(readLines("DESCRIPTION"), collapse = "")
stree <- new(suffix_tree, src)
# install.packages('microbenchmark')
sus.ctoqa <- stree$sus_ctoqa()
library(microbenchmark)
microbenchmark(c(sus.ctoqa$head[100], sus.ctoqa$tail[100]), stree$sus_baseline(100))
```

```
## Unit: nanoseconds
##                                         expr   min    lq median    uq
##  c(sus.ctoqa$head[100], sus.ctoqa$tail[100])   892  1128   1596  1738
##                      stree$sus_baseline(100) 18195 18915  19090 19456
##     max neval
##   14946   100
##  212913   100
```



