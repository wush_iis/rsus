/*

Copyright (C) 2013 Wush Wu <wush978@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/
#include "libstree.h"
#include <Rcpp.h>

using namespace Rcpp;

typedef std::vector<LST_Edge*> LeafArray;

class suffix_tree {
	LST_STree* ptree;
	LeafArray leaf_array;
public:
	suffix_tree(const std::string& src) {
		BEGIN_RCPP
		LST_StringSet *set(lst_stringset_new());
		LST_String *pstring(lst_string_new( (void*) src.c_str(), 1, src.size()));
		lst_stringset_add( set, pstring );
		ptree = lst_stree_new(set);
		leaf_array.resize(src.size());
		build_leaf_array();
		VOID_END_RCPP
	}
	
	~suffix_tree() {
		lst_stree_free(ptree);
	}
	
	SEXP search(std::string src) {
		BEGIN_RCPP
		const char 
			*start = src.c_str(), 
			*end = src.c_str() - 1,
			*stop = &src[src.size()];
		LST_Node* node(ptree->root_node);
		LST_Edge* edge(NULL);
		while(start != stop) {
//			Rprintf("%s \n", start);
			bool is_match = true;
			for (edge = node->kids.lh_first;edge;edge = edge->siblings.le_next) {
//				Rprintf("edge: %p %p \"%s\" \n", edge, edge->range.string, edge->range.string->data + edge->range.start_index);
				is_match = true;
				const char* data = reinterpret_cast<const char*>(edge->range.string->data);
				end = start;
				for(int i = edge->range.start_index; i <= edge->range.end_index[0]; i++) {
					if (end == stop) 
						break;
//					Rprintf("data[%d]:%c end[0]:%c \n", i, data[i], *end);
					if (data[i] != *(end++)) {
						if (end - start == 1) {
							is_match = false;
							break;
						}
						else {
//							Rprintf("mismatch!\n");
							return R_NilValue;
						}
					}
				}
				if (is_match) { // in for
//					Rprintf("match\n");
					break;
				}
			} // end for
			if (!edge) {
				return R_NilValue;
			}
			if (!is_match ) { // in while
				return R_NilValue;
			}
			start = end;
			node = edge->dst_node;
		} // end while
		node = edge->dst_node; // find all leaf under this node
		std::vector<LST_Node*> node_stack(1, node);
		std::vector<int> retval;
		while(node_stack.size()) {
			node = node_stack.back();
			node_stack.pop_back();
			if (!node->kids.lh_first) { // node is leaf
				retval.push_back(node->index + 1);
			}
			for (edge = node->kids.lh_first;edge;edge = edge->siblings.le_next) {
				if (edge->dst_node->kids.lh_first) {
					node_stack.push_back(edge->dst_node);
				}
				else {
					retval.push_back(edge->dst_node->index + 1);
				}
			}
		}
		return wrap<std::vector<int> >(retval);
		END_RCPP
	}

	SEXP lsus(IntegerVector index) {
		BEGIN_RCPP
		IntegerVector retval(index.size());
		for(int i = 0;i < index.size();i++) {
			if (index[i] > leaf_array.size()) throw std::invalid_argument("lsus: index out of range");
			if (index[i] < 1) throw std::invalid_argument("lsus: index out of range");
			retval[i] = lsus_internal(index[i] - 1);
		}
		return retval;
		END_RCPP
	}
	
	SEXP sus_baseline(IntegerVector index) {
		BEGIN_RCPP
		IntegerVector retval_head(index.size()), retval_tail(index.size());
		for(int i = 0;i < index.size();i++) {
			sus_baseline_internal(index[i] - 1, &retval_head[i], &retval_tail[i]);
			retval_tail[i]++;
			retval_head[i]++;
		}
		List retval;
		retval["head"] = retval_head;
		retval["tail"] = retval_tail;
		return retval;
		END_RCPP
	}
	
SEXP sus_ctoqa() {
	BEGIN_RCPP
	IntegerVector retval_head(leaf_array.size()), retval_tail(leaf_array.size());
	sus_ctoqa_internal(&retval_head[0], &retval_tail[0]);
	for(int i = 0;i < leaf_array.size();i++) {
		retval_head[i]++;
		retval_tail[i]++;
	}
	List retval;
	retval["head"] = retval_head;
	retval["tail"] = retval_tail;
	return retval;
	END_RCPP
}
	
private:
	void build_leaf_array() {
		std::vector<LST_Node*> node_stack(1, ptree->root_node);
		LST_Edge* edge(NULL);
		LST_Node* node(NULL);
		while(node_stack.size()) {
			node = node_stack.back();
			node_stack.pop_back();
			for (edge = node->kids.lh_first;edge;edge = edge->siblings.le_next) {
				if (edge->dst_node->kids.lh_first) {
					node_stack.push_back(edge->dst_node);
				}
				else {
					if (edge->dst_node->index < leaf_array.size())
						leaf_array[edge->dst_node->index] = edge;
				}
			}
		}
	}

	inline const bool isEOF(LST_StringIndex index) {
		return index.start_index == index.string->num_items - 1;
	}

	inline int lsus_internal(int i) {
		LST_StringIndex temp = leaf_array[i]->range;
		if (isEOF(temp)) {
			return NA_INTEGER;
		}
		return leaf_array.size() - i + 1 - *(temp.end_index) + temp.start_index;
	}
	
	void sus_baseline_internal(int i, int* head, int* tail) {
		*head = i;
		int min_len = leaf_array.size();
		int temp_head = i;
		int len;
		while(temp_head >= 0 & i - temp_head + 1 <= min_len) {
			len = lsus_internal(temp_head);
			if (len != NA_INTEGER) {
				if (len < i - temp_head + 1) {
					len = i - temp_head + 1;
				}
				if (len <= min_len) {
					min_len = len;
					*head = temp_head;
					*tail = temp_head + len - 1;
				}
			}
			temp_head--;
		}
	}
	
	void sus_ctoqa_internal(int* head, int* tail) {
		std::vector<int> p_candidate_head(leaf_array.size(), NA_INTEGER);
		std::vector<int> p_candidate_tail(leaf_array.size(), NA_INTEGER);
		int index = 0;
		head[index] = index;
		tail[index] = lsus_internal(index) - 1;
		int l, r, i, j, lsus_len;
		for(index = 1;index < leaf_array.size();index++) {
			lsus_len = lsus_internal(index); // length of lsus
			if (lsus_len == NA_INTEGER) { // lsus does not exist
				l = 0;
				r = leaf_array.size() - 1;
			}
			else {
				l = index;
				r = index + lsus_len - 1;
			}
			head[index] = l;
			tail[index] = r;
			
//			Rprintf("Init sus(%d) as (%d,%d) \n", index + 1, l + 1, r + 1);
			
			// compare with p_candidate
			if (p_candidate_head[index] != NA_INTEGER) {
				if (compare(
					p_candidate_head[index], 
					p_candidate_tail[index],
					head[index],
					tail[index]
				)) {
					head[index] = p_candidate_head[index];
					tail[index] = p_candidate_tail[index];
					
//					Rprintf("Update sus(%d) by p_candidate as (%d,%d) \n", index + 1, head[index] + 1, tail[index] + 1);
					
				}
			}
			
			// compare (head[index-1],index)
			if (compare(
				head[index - 1], 
				std::max(index, tail[index-1]),
					head[index],
					tail[index]
			)) {
				head[index] = head[index - 1];
				tail[index] = std::max(index, tail[index-1]);
				
//				Rprintf("Update sus(%d) by i_prev as (%d,%d) \n", index + 1, head[index] + 1, tail[index] + 1);

			}
			
			// propogation
			if (p_candidate_head[index] != NA_INTEGER & !is_equal(
				head[index], tail[index], 
				p_candidate_head[index], p_candidate_tail[index]
			)) {
				propogate(p_candidate_head[index], p_candidate_tail[index], tail[index] + 1,
					p_candidate_head, p_candidate_tail
				);
			}
			
			if (lsus_is_mus(index, lsus_len) & !is_equal(
				head[index], tail[index], 
				index, index + lsus_len - 1
			)) {
				propogate(index, index+lsus_len-1, tail[index] + 1,
					p_candidate_head, p_candidate_tail
				);
			}
		}
	}
	
	inline bool compare(int head1, int tail1, int head2, int tail2) {
		if (tail1 - head1 == tail2 - head2) 
			return head1 < head2;
		return tail1 - head1 < tail2 - head2;
	}
	
	inline bool is_equal(int head1, int tail1, int head2, int tail2) {
		return (head1 == head2) & (tail1 == tail2);
	}
	
	bool lsus_is_mus(int index, int lsus_len) {
		if (lsus_len == NA_INTEGER)
			return false;
		if (index == leaf_array.size() - 1)
			return true;
		return lsus_internal(index + 1) != lsus_len - 1;
	}
	
	void propogate(int head, int tail, int index, 
		std::vector<int>& p_candidate_head, std::vector<int>& p_candidate_tail
	) {
		if (index >= leaf_array.size()) {
			return;
		}
		if (index < head | index > tail) {
			return;
		}
		
//		Rprintf("propogate: (%d, %d) ==> p.cand(%d) \n", head+1, tail+1, index+1);
		
		if (p_candidate_head[index] == NA_INTEGER) {
			p_candidate_head[index] = head;
			p_candidate_tail[index] = tail;
			return;
		}
		int next_head, next_tail, next_index;
		if (compare(head, tail, p_candidate_head[index], p_candidate_tail[index])) {
			next_head = p_candidate_head[index];
			next_tail = p_candidate_tail[index];
			next_index = tail + 1;
			p_candidate_head[index] = head;
			p_candidate_tail[index] = tail;
		} else {
			next_head = head;
			next_tail = tail;
			next_index = p_candidate_tail[index] + 1;
		}
		return propogate(next_head, next_tail, next_index,
			p_candidate_head, p_candidate_tail
		);
	}
};

RCPP_MODULE(libstree) {
	
	class_<suffix_tree>("suffix_tree")
	.constructor<std::string>()
	.method("lsus", &suffix_tree::lsus)
	.method("search", &suffix_tree::search)
	.method("sus_baseline", &suffix_tree::sus_baseline)
	.method("sus_ctoqa", &suffix_tree::sus_ctoqa)
	;
	
}